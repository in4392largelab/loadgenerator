public class BenchMarker {

    private int numberOfThreads;
    private String url;

    public BenchMarker(int numberOfThreads, String url) {
        this.numberOfThreads = numberOfThreads;
        this.url = url;
    }

    public void startBenchMarking() {
        LoadCreatorThread loadCreatorThread = new LoadCreatorThread(url);
        ThreadGroup threadGroup1 = new ThreadGroup("Benchmarking group 1");
        while (true) {
            Thread thread = new Thread(threadGroup1, loadCreatorThread);
            thread.start();
            if (threadGroup1.activeCount() == numberOfThreads) {
                System.out.println("Thread count reached " + numberOfThreads + ". Waiting all threads to finish...");
                while (threadGroup1.activeCount() != 0) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Continuing...");
            }
        }
    }
}
