import org.jsoup.Jsoup;
import java.io.IOException;


public class LoadCreatorThread extends Thread{

    private String urlToConnect;

    public LoadCreatorThread(String urlToConnect) {
        this.urlToConnect = urlToConnect;
    }

    public void run() {
        System.out.println("Running load on server");
        try {
            Jsoup.connect(urlToConnect).timeout(0).get();
            System.out.println("Got response from server!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
